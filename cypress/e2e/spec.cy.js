
describe('KOMPAS.COM', () => {
  const Url= "https://kompas.com"
  beforeEach(() => {
    cy.viewport('macbook-16');
    cy.visit(Url);
    cy.wait(2000);
  });
  describe('Test Case', () => {
    it('Login Kompas', () => {
      cy.get('[id="sso__icon__login_top"]').click();
      cy.get('[id="txt_signin"]').click();
      //input email dan password
      cy.get('[id="email"]').type('softwaretesting@gmail.com');
      cy.get('[id="password"]').type('Akuntes1');
      cy.get('[value="Login"]').click();
    })
    it('Search Berita', () => {
      cy.contains('Pemilu').click();
      //pencarian berita tentang
      cy.get('[name="ins"]').type('debat');
      cy.get('[class="icon icon-search"]').click();
      // klik detail berita ke-2
      cy.contains('Cak Imin: Presiden Punya Hak Menilai Debat, tapi Jangan Berpihak').click();
    })
  })
});